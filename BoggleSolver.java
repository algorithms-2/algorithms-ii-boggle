import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;
import java.util.HashSet;

public class BoggleSolver {

    private final Trie26 trieDict;
    private HashSet<String> foundWords;

    public BoggleSolver(String[] dictionary) {
        this.trieDict = new Trie26();
        for (String word : dictionary)
            if (word.length() > 2) this.trieDict.insert(word);
    }

    public Iterable<String> getAllValidWords(BoggleBoard board) {
        this.foundWords = new HashSet<>();
        int rows = board.rows();
        int cols = board.cols();
        boolean[][] visited = new boolean[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                resetVisited(visited);
                boggleDFS(board, row, col, this.trieDict.root(), visited);
            }
        }
        return this.foundWords;
    }

    private void boggleDFS(BoggleBoard board, int row, int col, Node prevNode,
                           boolean[][] visited) {
        char c = board.getLetter(row, col);
        visited[row][col] = true;
        Node newNode = this.trieDict.nextNode(c, prevNode);
        if (newNode != null) {
            if (newNode.hasValue()) {
                foundWords.add(newNode.value());
            }
            for (int[] cell : adj(row, col)
            ) {
                int x = cell[0], y = cell[1];
                if (validCell(board, x, y) && !visited[x][y]) {
                    boggleDFS(board, x, y, newNode, visited);
                }
            }
        }
        visited[row][col] = false;
    }

    private Bag<int[]> adj(int row, int col) {
        Bag<int[]> adj = new Bag<>();
        int[] rowValues = {
                row - 1, row - 1, row - 1,
                row, row,
                row + 1, row + 1, row + 1
        };
        int[] colValues = {
                col - 1, col, col + 1,
                col - 1, col + 1,
                col - 1, col, col + 1
        };
        for (int i = 0; i < rowValues.length; i++) {
            adj.add(new int[] { rowValues[i], colValues[i] });
        }
        return adj;
    }

    private boolean validCell(BoggleBoard board, int row, int col) {
        return row >= 0 && col >= 0
                && row < board.rows() && col < board.cols();
    }

    private void resetVisited(boolean[][] visitedMatrix) {
        for (int i = 0; i < visitedMatrix.length; i++) {
            Arrays.fill(visitedMatrix[i], false);
        }
    }

    public int scoreOf(String word) {
        return this.trieDict.contains(word) ? score(word.length()) : 0;
    }

    private int score(int wordLength) {
        if (wordLength < 5) return 1;
        else if (wordLength < 6) return 2;
        else if (wordLength < 7) return 3;
        else if (wordLength < 8) return 5;
        return 11;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard(args[1]);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            StdOut.println(word);
            score += solver.scoreOf(word);
        }
        StdOut.println("Score = " + score);
    }
}
