# Algorithms II Assignment 4: Boggle
Solution for assignment 4 of the Princeton University Algorithms II course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/boggle/specification.php).

The goal of this assignment was to create an algorithm that efficiently finds all possible words on a board of Boggle, a game played by connecting neighbouring letters in a rectangular grid to create valid English words.
This solution includes the core class BoggleSolver.java, which takes a text representation of a Boggle board as input and "solves" it by computing the possible words and total score of the board.
The classes Trie26.java and Node.java are a utility for the solver. They are used to build a 26-way trie as a time-efficient implementation of the dictionary used to check if strings are valid words in Boggle.

## Results  

Correctness:  13/13 tests passed

Memory:       3/3 tests passed

Timing:       9/9 tests passed

<b>Aggregate score:</b> 100.00%


## How to use
BoggleSolver.java can take a .txt file in the format specified in the assignment description as a command line argument, and the starter code also includes utilities to generate random Boggle boards for the solver to solve.
<b>NOTE:</b> Depends on the algs4 library used in the course.
