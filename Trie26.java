public class Trie26 {
    private final Node root;

    public Trie26() {
        this.root = new Node();
    }

    public Node root() {
        return root;
    }

    public void insert(String word) {
        Node x = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (!x.hasLinkTo(c)) {
                x.putChildAt(c);
            }
            if (c == 'Q') {
                i++;
                if (i == word.length() || word.charAt(i) != 'U') return;
            }
            x = x.childAt(c);
        }
        x.setValue(word);
    }

    public boolean contains(String word) {
        Node x = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (!x.hasLinkTo(c)) return false;
            x = x.childAt(c);
            if (c == 'Q') {
                i++;
                if (i == word.length() || word.charAt(i) != 'U') return false;
            }
        }
        return x.value() != null;
    }

    public Node nextNode(char c, Node prevNode) {
        return prevNode.hasLinkTo(c) ? prevNode.childAt(c) : null;
    }
}
