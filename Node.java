public class Node {

    private static final char START_RADIX = 'A';
    private String value;
    private final Node[] links;


    public Node() {
        this.value = null;
        this.links = new Node[26];
    }

    public String value() {
        return this.value;
    }

    public boolean hasValue() {
        return this.value != null;
    }

    public void setValue(String val) {
        this.value = val;
    }

    public boolean hasLinkTo(char c) {
        return links[c - START_RADIX] != null;
    }

    public Node childAt(char c) {
        return links[c - START_RADIX];
    }

    public void putChildAt(char c) {
        links[c - START_RADIX] = new Node();
    }
}
